
# https://github.com/puppeteer/puppeteer/blob/717d4a7ada6f82a8dce8f801a7136d15c3b17df1/docs/troubleshooting.md#running-on-alpine

# Installs latest Chromium package.
RUN apk update && apk upgrade && \
    apk add --no-cache ca-certificates 

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV CHROME_BIN=/usr/bin/chromium-browser
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

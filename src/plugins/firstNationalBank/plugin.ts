import {
  CPlugin,
  CPluginClient,
} from "@bettercorp/service-base/lib/interfaces/plugins";
import { Tools } from "@bettercorp/tools/lib/Tools";
import { FNBClientConfig } from "./sec.config";
import * as puppeteer from "puppeteer";
import { Browser } from "puppeteer";
import { FNBAccount, FNBMethods } from "../../ILib";

export interface Transaction {
  date: number;
  description: string;
  reference: string;
  serviceFee: number;
  amount: number;
  balance: number;
}

export class firstNationalBank extends CPluginClient<FNBClientConfig> {
  public readonly _pluginName: string = "firstNationalBank";

  getTransactions(account: FNBAccount): Promise<Array<Transaction>> {
    return this.emitEventAndReturn(FNBMethods.getTransactions, account, 120);
  }
}

export class Plugin extends CPlugin<FNBClientConfig> {
  private _LOCK: boolean = false;
  init(): Promise<void> {
    const self = this;
    return new Promise(async (resolve) => {
      self.onReturnableEvent(null, FNBMethods.getTransactions, (data) =>
        self.getTransactionsForAccount(data)
      );
      resolve();
    });
  }

  private async getTransactionsForAccount(
    account: FNBAccount
  ): Promise<Array<Transaction>> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      if (self._LOCK) return reject("Cannot run dual sessions...");
      self._LOCK = true;
      let errr: any = null;
      let transactions: Array<Transaction> = [];
      let startTime = new Date().getTime();
      self.log.info("START BROWSER");
      let browser: Browser;
      try {
        let pupConf: any = {
          //args: ["--no-sandbox", "--disable-gpu"],
          executablePath: process.env.CHROME_BIN || undefined,
          args: ["--no-sandbox", "--headless", "--disable-gpu"],
          //headless: false,
          //headless: /*puppeteerConfig.headless ||*/ true,
          //args: puppeteerConfig.args || undefined,
          //executablePath: puppeteerConfig.executablePath || undefined,
        };
        if (
          !Tools.isNullOrUndefined(
            (await this.getPluginConfig()).runDebugger
          ) &&
          (await this.getPluginConfig()).runDebugger!.headless !== true
        ) {
          self.log.error(
            "RUNNING IN DEBUG INSTANCE:::::::: IF YOU SEE THIS IN PROD YOU`VE MESSED UP ::: RUNNING HEADED"
          );
          pupConf = {
            executablePath: process.env.CHROME_BIN || undefined,
            args: ["--no-sandbox", "--disable-gpu"],
            headless: false,
          };
        }
        browser = await puppeteer.launch(pupConf);
        self.log.info("GO TO FNB [NEW PAGE]");
        let pages = await browser.pages();
        let page = pages.length > 0 ? pages[0] : await browser.newPage();
        page.on("error", (e) => {
          self.log.error(e);
        });
        self.log.info("GO TO FNB");
        await page.goto("https://www.fnb.co.za/", { timeout: 60000 });
        self.log.debug("WAIT FOR SELECTOR");
        await page.waitForSelector("#user", { timeout: 60000 });
        self.log.info("START LOGIN");
        await page.$eval(
          "#user",
          (el: any, u: any) => (el.value = u),
          account.username
        );
        await page.$eval(
          "#pass",
          (el: any, p: any) => (el.value = p),
          account.password
        );
        self.log.info("SUBMIT LOGIN");
        await page.click("#OBSubmit");

        self.log.debug("WAIT FOR NAV");
        await page.waitForNavigation({ timeout: 60000 });

        self.log.debug("WAIT FOR SELECTOR");
        await page.waitForSelector("#pageContent", { timeout: 60000 });

        await new Promise((r) => setTimeout(r, 5000));

        let migrationDoc = await page.evaluate(
          () => document.querySelector("#migrateSecUserLanding") !== null
        );
        self.log.info("CONFIRM USER DETAILS?" + migrationDoc);
        if (migrationDoc) {
          self.log.debug("CONFIRM USER DETAILS...");
          await page.evaluate(() => {
            document
              .querySelector("#footerButtonsContainer")!
              .children[0].children[0].setAttribute("id", "skip-user-details");
          });
          self.log.info("SKIP USER DETAIL CONFIRMATION");
          await page.click("#skip-user-details");
          self.log.debug("WAIT FOR SELECTOR");
          await page.waitForSelector("#pageContent", { timeout: 60000 });
          await new Promise((r) => setTimeout(r, 5000));
        }

        let marketingFrame = await page.evaluate(
          () => document.querySelector("#marketingFrame") !== null
        );
        self.log.info("MARKETING FRAME?" + marketingFrame);
        if (marketingFrame) {
          self.log.debug("MARKETING FRAME...");
          await page.evaluate(() => {
            document
              .querySelector("#footerButtonsContainer")!
              .children[0].children[0].setAttribute(
                "id",
                "skip-marketing-frame"
              );
          });
          self.log.info("SKIP MARKETING FRAME");
          await page.click("#skip-marketing-frame");
          self.log.debug("WAIT FOR SELECTOR");
          await page.waitForSelector("#pageContent", { timeout: 60000 });
          await new Promise((r) => setTimeout(r, 5000));
        }

        let businessPersonal = await page.evaluate(
          () => document.querySelector("#linkedProfilesLanding") !== null
        );
        self.log.info("PERSONAL/BUSINESS FRAME?" + businessPersonal);
        if (businessPersonal) {
          self.log.debug("PERSONAL/BUSINESS FRAME...");
          let businesses = await page.evaluate(() => {
            let setElemts = [];
            for (let colOfSelectors of document.querySelector(
              "#linkedProfilesTable_tableContent"
            )!.children) {
              if (!colOfSelectors.classList.contains("tableDataRow")) continue;
              let profileName = colOfSelectors.querySelector(
                'div[name="profileName"]'
              );
              if (profileName === null) {
                /*console.warn(
                  "Cannot find profile name element... we will try next one..."
                );*/
                continue;
              }
              let profileStatus =
                colOfSelectors.querySelector('div[name="status"]');
              if (profileStatus === null) {
                /*console.warn(
                  "Cannot find profile status element... we will try next one..."
                );*/
                continue;
              }
              if (profileStatus.innerHTML.toUpperCase().indexOf("ACTIVE") < 0) {
                /*console.error(
                  `Profile [${profileName.innerHTML.trim()}] isn't active... we will try next one...`
                );*/
                continue;
              }
              let link = colOfSelectors.querySelector(".eziLinks");
              if (link === null) {
                continue;
              }
              link = link.children[0];
              if (link === null || link === undefined) {
                continue;
              }
              link.classList.add("select-business-frame");
              let profileNameText = (profileName as any).innerText;
              if (typeof profileNameText !== "string") {
                profileNameText = profileName.innerHTML.replace(
                  /[\r\n\t]/g,
                  ""
                );
              }
              profileNameText = profileNameText.trim().toUpperCase();
              if (profileNameText[0] === "*")
                profileNameText = profileNameText.substring(1);

              link.classList.add(
                "select-business-frame-" + profileNameText.replace(/\W/g, "-")
              );
              setElemts.push({
                name: profileNameText,
                selector: profileNameText.replace(/\W/g, "-"),
              });
            }
            return setElemts;
          });
          if (businesses.length === 0) {
            throw "unable to find biz frame selector!";
          } else self.log.info("SETUP BIZ FRAME....");
          self.log.info("SELECT BUSINESS FRAME");
          await new Promise((r) => setTimeout(r, 1000));
          //console.log(businesses);
          //await new Promise((r) => setTimeout(r, 100000));
          let bizToSelect: {
            name: any;
            selector: any;
          } | null = businesses[0];
          if (account.accountProfileName) {
            bizToSelect = null;
            for (let biz of businesses) {
              if (biz.name === account.accountProfileName) {
                bizToSelect = biz;
                break;
              }
            }
          }
          if (bizToSelect === null) {
            throw "unable to find business/profile!";
          }
          await page.click(".select-business-frame-" + bizToSelect.selector);
          self.log.debug("WAIT FOR NAV");
          await new Promise((r) => setTimeout(r, 4000));
          //await page.waitForSelector("#pageContent", { timeout: 60000 });
        }

        marketingFrame = await page.evaluate(
          () => document.querySelector("#marketingFrame") !== null
        );
        self.log.info("MARKETING FRAME?" + marketingFrame);
        if (marketingFrame) {
          self.log.debug("MARKETING FRAME...");
          await page.evaluate(() => {
            document
              .querySelector("#footerButtonsContainer")!
              .children[0].children[0].setAttribute(
                "id",
                "skip-marketing-frame"
              );
          });
          self.log.info("SKIP MARKETING FRAME");
          await page.click("#skip-marketing-frame");
          self.log.debug("WAIT FOR SELECTOR");
          await page.waitForSelector("#pageContent", { timeout: 60000 });
          await new Promise((r) => setTimeout(r, 5000));
        }

        try {
          self.log.debug("WAIT FOR SELECTOR");
          await page.waitForSelector("#pageContent", { timeout: 60000 });
          await page.waitForSelector("#welcomeContainer", { timeout: 60000 });
          await new Promise((r) => setTimeout(r, 5000));
          self.log.info("FIND ACCOUNTS");
          self.log.info(
            await page.evaluate(() => {
              const liChangePageItems = document.querySelectorAll(
                "#headerButtons #shortCutLinks span"
              );
              for (let li of liChangePageItems) {
                if (li.innerHTML.trim().toLowerCase() === "accounts") {
                  li.setAttribute("id", "go-to-bank-account");
                  return "FOUND ACCOUNTS LINK";
                }
              }
              return "ISSUE FINDING ACCOUNTS LINK";
            })
          );
          self.log.info("GO TO ACCOUNTS");
          await page.click("#go-to-bank-account");
          //await new Promise((r) => setTimeout(r, 5000));
          self.log.debug("WAIT FOR SELECTOR1");
          await page.waitForSelector("#summary_of_account_balances", {
            timeout: 60000,
          });
          self.log.debug("WAIT FOR SELECTOR2");
          await page.waitForSelector("#accountNumber", { timeout: 60000 });

          self.log.info(`FIND ACCOUNT [${account.bankAccount}]`);
          let result = await page.evaluate((x) => {
            function cleanHTML(txt: string) {
              return txt.replace(/\\n/g, "").replace(/\\r/g, "").trim();
            }
            const bankAccountItems =
              document.querySelectorAll("#accountNumber");
            for (let li of bankAccountItems) {
              if (cleanHTML(li.textContent!).toLowerCase() === x) {
                li.parentElement!.parentElement!.parentElement!.parentElement!.children[0].children[0].children[0].children[0].children[0].setAttribute(
                  "id",
                  "go-to-bank-account-page"
                );
                return Promise.resolve(true);
              }
            }
            return Promise.resolve(false);
          }, account.bankAccount);
          if (!result) throw "UNABLE TO FIND ACCOUNT";
          self.log.debug("WAIT TO ACC");
          await page.waitForSelector("#go-to-bank-account-page", {
            timeout: 60000,
          });
          self.log.info("GO TO ACC");
          await page.click("#go-to-bank-account-page");
          await new Promise((r) => setTimeout(r, 5000));
          self.log.debug("WAIT FOR SELECTOR");
          await page.waitForSelector("#subTabsScrollable", { timeout: 60000 });
          await new Promise((r) => setTimeout(r, 5000));

          self.log.info("FIND TRANSACTS");
          let resultTransacts = await page.evaluate(() => {
            try {
              const bankAccountItem =
                document.querySelector("#subTabsScrollable");
              for (let li of bankAccountItem!.children) {
                if (li.children[0].innerHTML === "Transaction History") {
                  if (`${li.getAttribute("class")}`.indexOf("active") >= 0) {
                    return Promise.resolve(0);
                  }
                  li.setAttribute(
                    "id",
                    "go-to-bank-account-transaction-history"
                  );
                  return Promise.resolve(1);
                }
              }
            } catch (exc) {
              self.log.error(exc);
            }
            return Promise.resolve(-1);
          });
          if (resultTransacts < 0) throw "UNABLE TO FIND TRANSACTS";
          /*self.log.debug("WAIT TO TRANSACTS");
          await page.waitForSelector(
            "#go-to-bank-account-transaction-history",
            { timeout: 60000 }
          );*/
          if (resultTransacts === 1) {
            self.log.info("GO TO TRANSACTS");
            await page.click("#go-to-bank-account-transaction-history");
            await new Promise((r) => setTimeout(r, 5000));
            self.log.debug("WAIT FOR SELECTOR");
            await page.waitForSelector("#transactionHistoryTables_table", {
              timeout: 60000,
            });
          } else {
            self.log.info("ON TRANSACTS");
          }

          const moreRowsButton = await page.evaluate(
            () =>
              document.querySelector(
                '#footerButtonsContainer > .footerBtn > a[href="#"'
              ) !== null &&
              document.querySelector(
                '#footerButtonsContainer > .footerBtn > a[href="#"'
              )!.innerHTML === "More"
          );
          self.log.info("MORE ROWS BUTTON?" + moreRowsButton);
          if (moreRowsButton) {
            self.log.debug("MORE ROWS BUTTON...");
            await page.click('#footerButtonsContainer > .footerBtn > a[href="#"');
            await new Promise((r) => setTimeout(r, 5000));

          }

          self.log.info("FIND TRANSACT LIST");
          transactions = await page.evaluate(() => {
            function cleanHTML(txt: string) {
              return txt
                .replace(/\\n/g, "")
                .replace(/\\r/g, "")
                .replace(/[ ]{2,}/g, " ")
                .trim();
            }
            function cleanHTMLNumber(txt: string) {
              return cleanHTML(txt).replace(/,/g, "").trim();
            }
            try {
              const transactionItems = document.querySelectorAll(
                "#transactionHistoryTables_tableContent .tableRow.tableDataRow.tableRowGroup1"
              );
              let itransactions = [];
              for (let li of transactionItems) {
                let date: number = new Date(
                  cleanHTML(
                    li.children[0].children[0].children[0].children[0]
                      .children[0].textContent!
                  )
                ).getTime();
                let description: string = cleanHTML(
                  li.children[0].children[0].children[0].children[1].children[0]
                    .textContent!
                );
                let reference: string = cleanHTML(
                  li.children[0].children[1].children[0].children[0].children[0]
                    .textContent!
                );
                let serviceFee: number = Number.parseFloat(
                  cleanHTMLNumber(
                    li.children[0].children[1].children[0].children[1]
                      .children[0].textContent!
                  )
                );
                let amount: number = Number.parseFloat(
                  cleanHTMLNumber(
                    li.children[0].children[1].children[0].children[2]
                      .children[0].textContent!
                  )
                );
                let balance: number = Number.parseFloat(
                  cleanHTMLNumber(
                    li.children[0].children[1].children[0].children[3]
                      .children[0].textContent!
                  )
                );
                itransactions.push({
                  date,
                  description,
                  reference,
                  serviceFee,
                  amount,
                  balance,
                });
              }
              return Promise.resolve(itransactions);
            } catch (exc) {
              self.log.error(exc);
              return Promise.reject(exc);
            }
          });

          self.log.info("STORE TRANSACTS");
        } catch (exc) {
          self.log.error(exc);
          errr = exc;
        }
        try {
          self.log.info("LOGOUT");
          await page.click(".logoutBtn");
          self.log.debug("WAIT FOR NAV");
          await page.waitForNavigation({ timeout: 30000 });
          //await new Promise((r) => setTimeout(r, 5000));
        } catch (exc) {
          self.log.error(exc);
        }
        self.log.debug("CLOSE");
        await page.close();
      } catch (exc) {
        self.log.error("PUP ERROR");
        self.log.error(exc);
        self.log.fatal(exc);
        errr = exc;
      }
      self.log.info("PUP END");
      if (!Tools.isNullOrUndefined(browser!)) {
        self.log.info("CLOSE BROWSER");
        await browser!.close();
        await new Promise((r) => setTimeout(r, 5000));
      }

      self.log.info(
        `GET RUN TOOK : ${(new Date().getTime() - startTime) / 1000}s`
      );

      self._LOCK = false;

      if (!Tools.isNullOrUndefined(errr)) return reject(errr);
      self.log.info(
        `FOUND [${transactions.length}] TRANSACTIONS FOR ACCOUNT [${account.bankAccount}]`
      );
      resolve(transactions);
    });
  }

  async loaded(): Promise<void> {
    if (!Tools.isNullOrUndefined((await this.getPluginConfig()).runDebugger)) {
      this.log.error(
        "GOING TO RUN IN DEBUG INSTANCE:::::::: IF YOU SEE THIS IN PROD YOU`VE MESSED UP"
      );
      const self = this;
      setTimeout(async () => {
        self.emitEventAndReturn<FNBAccount, Array<Transaction>>(
          null,
          FNBMethods.getTransactions,
          (await self.getPluginConfig()).runDebugger,
          120000
        );
      }, 1000);
    }
  }
}

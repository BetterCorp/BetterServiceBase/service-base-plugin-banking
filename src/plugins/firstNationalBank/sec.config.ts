import { FNBAccount } from "../../ILib";

export interface FNBClientConfigDebugAccount extends FNBAccount {
  headless: boolean;
}
export interface FNBClientConfig {
  puppeteer: any;
  runDebugger?: FNBClientConfigDebugAccount; // can be ignored in normal use
}

export default (existingConfig?: FNBClientConfig): FNBClientConfig => {
  return {
    puppeteer: (existingConfig || {}).puppeteer || {
      headless: true,
      args: ["--disable-dev-shm-usage"],
      executablePath: "/usr/bin/chromium-browser",
    },
  };
};

export enum FNBMethods {
  getTransactions = "get-transactions",
}

export interface FNBAccount {
  username: string;
  password: string;
  bankAccount: string;
  accountProfileName?: string;
}